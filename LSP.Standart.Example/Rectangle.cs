﻿using System;
namespace LSP.Standart.Example
{
    public class Rectangle
    {
        public virtual int Width { get; set; }

        public virtual int Height { get; set; }

        public int GetArea()
        {
            return this.Width * this.Height;
        }
    }
}
