﻿namespace KISS.App.Problem
{
    public struct WeatherInfo
    {
        public double Temperature { get; set; }
    }
}