public class DatabaseInitializer
{
    public void InitializeMySqlDatabase() {}

    public void InitializeMsSqlDatabase() {}

    public void InitializeMongoDbDatabase() {}

    public void InitializePostgresDatabase() {}

    public void CheckConnection() {}

    public void CreateConnection() {}
}