﻿namespace DIP02.App.BusinessLogic
{
    public class CommandArguments
    {
        public const string Text = "--text";

        public const string DocumentName = "--name";
    }
}