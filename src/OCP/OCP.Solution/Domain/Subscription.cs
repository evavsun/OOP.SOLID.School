﻿namespace OCP.Solution.Domain
{
    public class Subscription
    {
        public bool IsActive { get; set; }
    }
}