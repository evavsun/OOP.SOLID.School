﻿namespace OCP.Solution.Domain
{
    public class Roles
    {
        public const string Admin = nameof(Admin);
        public const string User = nameof(User);
    }
}